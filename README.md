## Bienvenue sur le projet "PROMÉTHÉE":

Ce projet consiste à créer un *réseau social interne des apprentissages.*

Une fois connecté, l'utilisateur doit pouvoir rechercher les profils ou les compétences qu'il souhaite connaître. **Cette page représente l'annuaire**.

Il doit également avoir accès à son profil, dans une page "tableau de bord" qui lui est dédié, avec la possibilité de modifier ses informations personnelles et 'supprimer son compte' s'il le veut.

Le contenu est créé par les utilisateurs, au fur et à mesure des échanges qu'ils ont et des connaissances qu'ils acquérièrent ensemble.

## Objectif V.1:

- Créer un système d'authentification.

- Créer un annuaire dynamique et fonctionnel.

- Créer une page 'tableau de bord' statique avec possibilité de changer ses informations et de pouvoir supprimer le compte.

## Technique: 

1. Technologie principale : **Laravel**

2. Afin d'assurer une bonne gestion des rôles, des utilisateurs, de leurs permissions et toutes les statistiques du site, nous avons trouvé judicieux d'utiliser Voyager qui est un outil de gestion bien pratique.

Voici un lien vers les configurations possibles avec cet outil : [Configurations Voyager](https://voyager-docs.devdojo.com/getting-started/configurations)

## Pour lancer le projet en local: 

1. Cloner/télécharger le projet,

**Depuis le dossier projet**, écrire dans votre terminal:

2. ```composer install && npm install```  *(installation des dépendances nécessaires au fonctionnement des divers modules)*

3. Avec un SGBD, type "*MySQL Workbench*", créer une BDD (connexion & schema) vide pour l'instant.

Dans notre projet, le fichier ```.env``` stock toutes les données sensibles de notre application dans des variables d'environnement.
Il ne peut et ne doit donc en aucun cas être mis en ligne et doit être créé localement.

4. Dupliquer le ```.env.exemple```, renommer la copie en ```.env```, puis y renseigner nos informations.

Pour accèder à votre base de données, il faut renseigner :

    DB_DATABASE= *nom-de-la-BDD*
    DB_USERNAME= *nom-utilisateur-BDD*
    DB_PASSWORD= *mdp-BDD*


Il faut également définir la clés d'application, l'APP_KEY, cela se fait automatiquement avec la commande :

5. ```php artisan key:generate``` *(toujours depuis le dossier projet)*

On peut désormais initialiser les tables de la BDD:

6. ```php artisan migrate```

À ce stade, la base de donnée est prête et on ***peut*** lancer notre serveur :

```php artisan serve```

Cependant, pour gérer l'administration du site, 

7. il va falloir installer & configurer Voyager.

### Voyager

La mise en place notre tableau de bord administrateur nécessite l'installation de Voyager :

```php artisan voyager:install```

On va maintenant créer notre premier administrateur avec :
```php artisan voyager:admin your@email.com --create``` *(pensez à modifier l'email renseigné)*

Vous êtes invité à renseigner un nom (votre pseudo) et un mot de passe (permettant la connexion au tableau de bord admin) .

On peut maintenant lancer le serveur local : 

```php artisan serve``` 

Ouvrir l'URL : ```http://localhost:8000```

Connectez vous avez vos identifiants administrateur

Puis ajouter **/admin** à la suite de l'adresse locale.

Vous serez redirigé sur la plateforme de Voyager et devrez vous connecter avec le mail et le mot de passe renseigné auparavant.

**VOUS VOILA SUR VOTRE TABLEAU DE BORD ADMINISTRATEUR ! **

### NOTE DÉVELOPPEUR

À ce stade de développement, une erreur persiste au sein de Voyager. Pour accèder à l'onglet *DATABASE* dans le menu déroulant *Tools*, 
une solution est d'indiquer l'id de l'utilisateur dans un fichier Vendor de voyager.
Dans le fichier ***vendor/tcg/voyager/resources/views/tools/database/index.blade.php*** l.96

remplacer ['id' => null] par ['id' => Auth::user()->id]

## Voici nos liens utiles:

- [Notre Journal de bord](https://docs.google.com/document/d/1CWJIsHfJnfRdiqzE3uxtBZInhDpY8bnOHSv8mAUlA78/edit)
- [Taïga.io](https://tree.taiga.io/project/maxime_do-promethee/backlog)

Voici également deux exemples d'hébergeurs pour la suite:

- [OVH](https://www.ovh.com/fr/hebergement-web/quel_hebergement_mutualise_choisir.xml)
- [Heroku](https://www.heroku.com/)
