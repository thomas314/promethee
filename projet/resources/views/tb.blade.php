@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-5 tb-header">
        <div class="col-2">badges</div>
        <div class="col">notifications</div>
        <div class="col">objectifs</div>
        <div class="col">amies</div>
        <div class="col">messagerie</div>
    </div>
        <div class="row mb-4">
            <div class="col">
              <img src="{{-- $avatar --}}" alt="photo de profil" class="img-thumbnail"/>
                <form method="POST" action="/img-upload" enctype="multipart/form-data">
                    @csrf
                    <!-- On limite le fichier à 200Ko -->
                    <input type="hidden" name="MAX_FILE_SIZE" value="200000">
                    <input type="file" name="avatar">
                    <button type="submit">Modifer</button>
                </form>
               
                <p>Nom / Pseudo: {{ Auth::User()->name }}</p>
                <p>Mes disponibilités</p>
                <p>Ateliers réalisés / suivis</p>
            </div>
            <div class="col">
                <h2 class="mb-4">Profil</h2>
                <p>Mes connaissances & compétences</p>
                <p>Ateliers prévus</p>
            </div>
            <div class="col">
                <p>Demandes</p>
            </div>
        </div>
        
        <div class="row no-gutters">
            {{-- <div class="col"> --}}
                <p>Suivi d'activité / Temps de connexion</p>
            {{-- </div> --}}
        </div>
        
        <div class="row">
            <div class="col">Recommandations reçues</div>
            <div class="col">Recommandations d'autres membres</div>
        </div>       
    </div>
</div>
@endsection
