@extends('layouts.app')
@section('content')
<div class="container">
        <div class="row">
            <div class="container">
                <h3>Vous cherchez un profil, une connaissance ou une compétence en particulier?</h3>
            </div>
        </div>
    </div>
    <div class="row p-md-4">
    <div class="container col">
        <div class="column">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Connaissances/Compétences</h5>
                    <div class="d-flex flex-row bd-highlight mb-3">
                        <div class="card-rows p-md-2">
                            <h6 class="card-title">Lorem</h6>
                                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi iure esse est harum, sint veritatis ipsam earum natus mollitia numquam ratione molestiae deserunt modi voluptatem atque eaque neque vero tempora.</p>
                            <button type="button" class="btn btn-primary">Choisir</button>
                        </div>
                        <div class="card-rows p-md-2">
                                <h6 class="card-title">Lorem</h6>
                                    <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi iure esse est harum, sint veritatis ipsam earum natus mollitia numquam ratione molestiae deserunt modi voluptatem atque eaque neque vero tempora.</p>
                                <button type="button" class="btn btn-primary">Choisir</button>
                        </div>
                        <div class="card-rows p-md-2">
                            <h6 class="card-title">Lorem</h6>
                                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi iure esse est harum, sint veritatis ipsam earum natus mollitia numquam ratione molestiae deserunt modi voluptatem atque eaque neque vero tempora.</p>
                            <button type="button" class="btn btn-primary">Choisir</button>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <div class="container col-3" >
            <form method="post" action="/findSearch">
                <input type="text" name="search">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button>Rechercher</button>
            </form>
    </div>
</div>
           <div class="container d-flex flex-row bd-highlight mb-3">
                <div class="column">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ Auth::user()->name }}</h5>
                                <p class="card-text">Pour le contacter, voici ses coordonnées: <br> {{ Auth::user()->email }}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Voir le profil</button>

                                <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Profil</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-profil">
                                        <h3>{{ Auth::user()->name }}</h3>
                                        <div class="container">
                                        <p>Badge</p>
                                        <img src="#" alt="Photo d'identité">
                                        </div>
                                        </div class="container">
                                        <p>Ateliers</p>
                                        <div class="d-flex justify-content-between">
                                            <div class="mx-auto">
                                            <p>A venir</p>
                                            <button type="button" class="btn btn-primary">S'inscrire</button>
                                            <p>A venir</p>
                                            <button type="button" class="btn btn-primary">S'inscrire</button>
                                            <p>A venir</p>
                                            <button type="button" class="btn btn-primary">S'inscrire</button>
                                            </div>
                                            <div class="mx-auto">
                                            <p>Passé</p>
                                            <button type="button" class="btn btn-primary">Evaluer</button>
                                            <p>Passé</p>
                                            <button type="button" class="btn btn-primary">Evaluer</button>
                                            <p>Passé</p>
                                            <button type="button" class="btn btn-primary">Evaluer</button>
                                            </div>
                                        </div>
                                    <div class="container">
                                        <p>Connaissances/Compétences maitrisées</p>
                                        <div class="container_cptce">
                                                <button type="button" class="btn btn-primary">Ajouter connaissances/competences</button>
                                        </div>
                                        <p>Reconnaissances</p>
                                        <div class="container_reco">
                                                <button type="button" class="btn btn-primary">Recommander</button>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Contacter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- {{ $errors->links() }} --}}
            </div>
        </div>
    </div>

@endsection
