<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Prométhée') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/home') }}">
                {{ config('app.name', 'Accueil') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                @guest
                    {{-- guest right nav items --}}
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item btn">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Se connecter') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item btn">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('S\'enregistrer') }}</a>
                        </li>
                        @endif
                @else
                    {{-- guest middle nav items --}}
                    <ul class="navbar-nav">
                        <li class="nav-item pl-5 d-flex align-items-center"><a class="nav-link" href="#">Demandes</a></li>
                        <li class="nav-item pl-2 d-flex align-items-center"><a class="nav-link" href="/annuaire">Annuaire</a></li>
                        <li class="nav-item pl-2 d-flex align-items-center"><a class="nav-link" href="#">Discussions</a></li>
                        <li class="nav-item pl-2 d-flex align-items-center"><a class="nav-link" href="#">Formations</a></li>
                        <li class="nav-item pl-2 d-flex align-items-center"><a class="nav-link" href="#">Amis</a></li>
                        <li class="nav-item pl-2 d-flex align-items-center"><a class="nav-link" href="#">Alertes</a></li>
                        <li class="nav-item pl-2 d-flex align-items-center"><a class="nav-link" href="#">Temps de connexion</a></li>
                        @admin
                        <li class="nav-item pl-2 d-flex align-items-center">
                            <a class="nav-link" href="/admin" role="button" v-pre>
                                Administration <span class="caret"></span>
                            </a>
                        </li>
                        @endadmin

                        {{-- Dropdown Links --}}
                        <li class="nav-item pl-2 d-flex align-items-center dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="/tb" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/tb">Profil</a>
                                <a class="dropdown-item" href="/param">Paramètres</a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Déconnexion') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('warning'))
    <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('warning') }}</p>
    @endif

    <main class="py-4">
        @yield('content')

    </main>

    @auth
      <!-- Footer -->
     <footer class="fixed-bottom bg-light shadow-lg">
        <div class="container d-flex justify-content-around">
            <li class="nav-item btn"><a href="https://www.service-public.fr/professionnels-entreprises/vosdroits/F31228">Mention Légale</a></li>
            <li class="nav-item btn"><a href="#">CGU</a></li>
            <li class="nav-item btn"><a href="#">CGV</a></li>
            <li class="nav-item btn"><a href="#">Contact</a></li>
            <li class="nav-item btn"><a href="#">facebook</a></li>
            <li class="nav-item btn"><a href="#">twitter</a></li>
            <li class="nav-item btn"><a href="#">linkedin</a></li>
        </div>
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3 bg-dark text-light">© 2019 Copyright:
            <a href="/home"> Promethee.com</a>
        </div>
    </footer>
    @else
    @endauth

</body>
</html>
