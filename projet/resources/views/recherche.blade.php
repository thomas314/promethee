@extends('layouts.app')
@section('content')

<div class="container d-flex justify-content-end" >
    <form method="post" action="/findSearch">
        <input type="text" name="search">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button>Rechercher</button>
    </form>
</div>


@if (isset($details))


    <div class="container">

     @foreach($details as $user)
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ $user->name }}</h5>
                        <p class="card-text">Pour le contacter, voici ses coordonnées: <br> {{$user->email}}</p>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Voir le profil</button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Profil</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-profil">
                                        <h3>{{$user->name}}</h3>
                                        <div class="container">
                                        <p>Badge</p>
                                        <img src="#" alt="Photo d'identité">
                                        </div>
                                        </div class="container">
                                        <p>Ateliers</p>
                                        <div class="d-flex justify-content-between">
                                            <div class="mx-auto">
                                            <p>A venir</p>
                                            <button type="button" class="btn btn-primary">S'inscrire</button>
                                            <p>A venir</p>
                                            <button type="button" class="btn btn-primary">S'inscrire</button>
                                            <p>A venir</p>
                                            <button type="button" class="btn btn-primary">S'inscrire</button>
                                            </div>
                                            <div class="mx-auto">
                                            <p>Passé</p>
                                            <button type="button" class="btn btn-primary">Evaluer</button>
                                            <p>Passé</p>
                                            <button type="button" class="btn btn-primary">Evaluer</button>
                                            <p>Passé</p>
                                            <button type="button" class="btn btn-primary">Evaluer</button>
                                            </div>
                                        </div>
                                    <div class="container">
                                        <p>Connaissances/Compétences maitrisées</p>
                                        <div class="container_cptce">
                                                <button type="button" class="btn btn-primary">Ajouter connaissances/competences</button>
                                        </div>
                                        <p>Reconnaissances</p>
                                        <div class="container_reco">
                                                <button type="button" class="btn btn-primary">Recommander</button>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Contacter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

@endif




@endsection
