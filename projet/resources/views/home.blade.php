@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="text-center">Accueil</h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm text-center">
            <div class="card">
                <div class="card-header">
                    Info sur la communauté
                </div>
                <div class="card-body">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit suscipit atque porro nemo molestias. Et, perspiciatis voluptatem fugit tempore modi error nostrum sequi sapiente doloribus? Dicta enim iure nam laudantium?
                </div>
            </div>
        </div>
        <div class="col-sm text-center">
            <div class="card">
                <div class="card-header">
                    Membre de la communauté
                </div>
                <div class="card-body">
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit suscipit atque porro nemo molestias. Et, perspiciatis voluptatem fugit tempore modi error nostrum sequi sapiente doloribus? Dicta enim iure nam laudantium?
                </div>
            </div>
        </div>
        <div class="col-sm text-center">
            <div class="card">
                <div class="card-header">
                        Savoir / Savoir-faire
                </div>
                <div class="card-body">
                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit suscipit atque porro nemo molestias. Et, perspiciatis voluptatem fugit tempore modi error nostrum sequi sapiente doloribus? Dicta enim iure nam laudantium?
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <div class="card">
                    <div class="card-header">
                        Articles
                    </div>
                    <div class="card-body">
                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit suscipit atque porro nemo molestias. Et, perspiciatis voluptatem fugit tempore modi error nostrum sequi sapiente doloribus? Dicta enim iure nam laudantium?
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
