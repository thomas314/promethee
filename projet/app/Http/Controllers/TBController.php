<?php

namespace App\Http\Controllers;

use App\Role;
use App\User_role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TBController extends Controller
{
    public function index()
    {
        $url_picture = 'storage/' . Auth::user()['avatar'];

        return view('tb', [
            'avatar' => $url_picture
        ]);
    }
    
    public function param()
    {
        return view('param');
    }
    public function upload(Request $request)
    {
        $dossier = 'storage/' . Auth::user()['avatar'];
        $fichier = basename($_FILES['avatar']['name']);
        $taille_maxi = 200000;
        $taille = filesize($_FILES['avatar']['tmp_name']);
        // récupérer le type MIME serait préférable
        $extensions = array('.png', '.gif', '.jpg', '.jpeg');
        $extension = strrchr($_FILES['avatar']['name'], '.'); 
        //Début des vérifications de sécurité...
        if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
        {
             $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg';
        }
        if($taille == false)
        {
             $erreur = 'Le fichier est trop gros : 200 Ko maximum';
        }
        if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
        {
             //On formate le nom du fichier ici...
             $fichier = strtr($fichier, 
                  'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
                  'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
             $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
             if(move_uploaded_file($_FILES['avatar']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
             {
                return back()->with('message', 'Upload effectué avec succès !');
             }
             else //Sinon (la fonction renvoie FALSE).
             {
                return back()->with('warning', 'Echec de l\'upload !');
             }
        }
        else
        {
            return back()->with('warning', $erreur);
        }
    }
}