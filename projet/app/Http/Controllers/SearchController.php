<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\User;

class SearchController extends Controller
{
    public function findSearch()
    {
        $search = request()->input("search");
        $user = User::where('name', 'LIKE', '%' . $search . '%')->get();
        // $knowledge = Knowledge::where('name', 'LIKE', '%', $search . '%')->get();
        if (count($user) > 0){
            return view('/recherche', ["users" => $user])->withDetails($user)->withQuery($search);
        }
        else
            return view('annuaire')->withMessage('No Details found. Try to search again !');
        // if (count($knowledge) > 0)
        //     return view('search')->withDetails($knowledge)->withQuery($search);
        // else
        //     return view('search')->withMessage('No Details found. Try to search again !');
    }
}
