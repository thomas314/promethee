<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/', 'AnnuaireController@index'); // temporaire V.1
    Route::get('/home', 'HomeController@index'); //
    Route::get('/tb', 'TBController@index');
    Route::get('/param', 'TBController@param');
    Route::get('/annuaire', 'AnnuaireController@index');

    Route::post('/findSearch', 'SearchController@findSearch');
    Route::post('/img-upload', 'TBController@upload');
    Route::post('/param_user', 'UserController@update');
    Route::post('/del_user', 'UserController@destroy');
    Route::view('/recherche', 'recherche');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

